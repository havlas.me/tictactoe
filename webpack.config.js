'use strict'

const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

module.exports = {
    mode: 'development',
    entry: [
        path.resolve(__dirname, './src/index.js'),
    ],
    output: {
        filename: 'static/js/[name].bundle.js',
        publicPath: '/',
    },
    devServer: {
        static: path.resolve(__dirname, './public'),
        historyApiFallback: true,
        port: 3000,
    },
    resolve: {
        exportsFields: [],
    },
    module: {
        rules: [
            {
                oneOf: [
                    {
                        test: /\.(js|jsx)$/,
                        include: [
                            path.resolve(__dirname, './src'),
                        ],
                        use: [
                            {
                                loader: require.resolve('babel-loader'),
                            },
                        ],
                    },
                    {
                        test: /\.(c|sa|sc)ss$/,
                        sideEffects: true,
                        use: [
                            {
                                loader: require.resolve('style-loader'),
                            },
                            {
                                loader: require.resolve('css-loader'),
                                options: {
                                    importLoaders: 2,
                                    sourceMap: true,
                                },
                            },
                            {
                                loader: require.resolve('sass-loader'),
                                options: {
                                    sassOptions: {
                                        precision: 8,
                                        outputStyle: 'compressed',
                                    },
                                    sourceMap: true,
                                },
                            },
                        ],
                    },
                    {
                        exclude: [/\.(js|jsx)$/, /\.html$/, /\.json$/],
                        use: [
                            {
                                loader: require.resolve('file-loader'),
                                options: {
                                    name: 'static/media/[name].[hash:8].[ext]',
                                },
                            },
                        ],
                    },
                ],
            },
        ],
    },
    plugins: [
        new webpack.ProvidePlugin({
            React: 'react'
        }),
        // generate an index.html with the <script> injected
        new HtmlWebPackPlugin({
            filename: './index.html',
            inject: true,
            template: './public/index.html',
        }),
    ],
}
