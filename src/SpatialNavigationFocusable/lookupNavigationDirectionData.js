export default {
    'ArrowDown': 'focusableNextSouth',
    'ArrowLeft': 'focusableNextWest',
    'ArrowRight': 'focusableNextEast',
    'ArrowUp': 'focusableNextNorth',
}
