import PropType from 'prop-types'

export default {
    /**
     * the children
     */
    children: PropType.node,
    /**
     * the focus index
     */
    index: PropType.string.isRequired,
    /**
     * the next focus index, in right direction
     */
    nextFocusableEast: PropType.string,
    /**
     * the next focus index, in up direction
     */
    nextFocusableNorth: PropType.string,
    /**
     * the next focus index, in down direction
     */
    nextFocusableSouth: PropType.string,
    /**
     * the next focus index, in left direction
     */
    nextFocusableWest: PropType.string,
    /**
     * the tab index
     * default: 0
     */
    tabIndex: PropType.number,
}
