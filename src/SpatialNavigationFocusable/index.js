import { defaultTo, isNil } from 'ramda'
import React, { useCallback } from 'react'
import getKeyboardEventKey from '../WebTvNativeApp/getKeyboardEventKey'
import propTypes from './index.type'
import lookupNavigationDirection from './lookupNavigationDirection'
import lookupNavigationDirectionData from './lookupNavigationDirectionData'

const lookupNextElement = function (index) {
    return document.querySelector(`[data-focusable-component-index="${index}"]`)
}

const focusNextElement = function (element, direction) {
    if (!element?.disabled) element?.focus()
    else if (!isNil(direction) && !isNil(element?.dataset?.[direction])) focusNextElement(lookupNextElement(element.dataset[direction]), direction)
}

/**
 * The SpatialNavigationFocusable component.
 *
 * @return {JSX.Element}
 * @constructor
 */
function SpatialNavigationFocusable ({ children, index, tabIndex = 0, ...rest }) {
    const onKeyDown = useCallback(function (event) {
        if (!isNil(rest[lookupNavigationDirection[getKeyboardEventKey(event)]])) {
            event.stopPropagation()
            focusNextElement(lookupNextElement(rest[lookupNavigationDirection[getKeyboardEventKey(event)]]), lookupNavigationDirectionData[getKeyboardEventKey(event)])
        }
    }, [rest])

    return React.cloneElement(children, {
        onKeyDown, tabIndex,
        'data-focusable-component-index': index,
        'data-focusable-next-east': defaultTo(void 0, rest?.nextFocusableEast),
        'data-focusable-next-north': defaultTo(void 0, rest?.nextFocusableNorth),
        'data-focusable-next-south': defaultTo(void 0, rest?.nextFocusableSouth),
        'data-focusable-next-west': defaultTo(void 0, rest?.nextFocusableWest),
    })
}

SpatialNavigationFocusable.propTypes = propTypes

export default SpatialNavigationFocusable
