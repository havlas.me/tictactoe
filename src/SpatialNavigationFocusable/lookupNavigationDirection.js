export default {
    'ArrowDown': 'nextFocusableSouth',
    'ArrowLeft': 'nextFocusableWest',
    'ArrowRight': 'nextFocusableEast',
    'ArrowUp': 'nextFocusableNorth',
}
