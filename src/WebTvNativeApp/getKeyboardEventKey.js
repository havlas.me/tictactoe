const normalizeKey = {
    Esc: 'Escape',
    Spacebar: ' ',
    Left: 'ArrowLeft',
    Up: 'ArrowUp',
    Right: 'ArrowRight',
    Down: 'ArrowDown',
    Del: 'Delete',
    Win: 'OS',
    Menu: 'ContextMenu',
    Apps: 'ContextMenu',
    Scroll: 'ScrollLock',
    MozPrintableKey: 'Unidentified',
}

const translateToKey = {
    '8': 'Backspace',
    '9': 'Tab',
    '12': 'Clear',
    '13': 'Enter',
    '16': 'Shift',
    '17': 'Control',
    '18': 'Alt',
    '19': 'Pause',
    '20': 'CapsLock',
    '27': 'Escape',
    '32': ' ',
    '33': 'PageUp',
    '34': 'PageDown',
    '35': 'End',
    '36': 'Home',
    '37': 'ArrowLeft',
    '38': 'ArrowUp',
    '39': 'ArrowRight',
    '40': 'ArrowDown',
    '45': 'Insert',
    '46': 'Delete',
    '112': 'F1',
    '113': 'F2',
    '114': 'F3',
    '115': 'F4',
    '116': 'F5',
    '117': 'F6',
    '118': 'F7',
    '119': 'F8',
    '120': 'F9',
    '121': 'F10',
    '122': 'F11',
    '123': 'F12',
    '144': 'NumLock',
    '145': 'ScrollLock',
    '224': 'Meta',
    '457': 'Info',
    '461': 'Backspace',
}

const getEventCharCode = function (event) {
    let charCode
    const keyCode = event.keyCode

    if ('charCode' in event) {
        charCode = event.charCode

        if (charCode === 0 && keyCode === 13) {
            charCode = 13
        }
    } else {
        charCode = keyCode
    }

    if (charCode === 10) {
        charCode = 13
    }

    if (charCode >= 32 || charCode === 13) {
        return charCode
    }

    return 0
}

const getKeyboardEventKey = function (event) {
    if (event.key && normalizeKey[event.key] !== 'Unidentified') {
        return normalizeKey[event.key] || event.key
    } else if (event.keyIdentifier && event.keyIdentifier[0] !== 'U' && event.keyIdentifier[1] !== '+' && normalizeKey[event.keyIdentifier] !== 'Unidentified') {
        return normalizeKey[event.keyIdentifier] || event.keyIdentifier
    }
    return translateToKey[event.keyCode] || String.fromCharCode(getEventCharCode(event))
}

export default getKeyboardEventKey
