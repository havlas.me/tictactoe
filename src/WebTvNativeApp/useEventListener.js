import { useEffect, useMemo } from 'react'
import addEventListener from './addEventListener'

/**
 * The useEventListener hook.
 *
 * @author Tomáš Havlas <havlas@bbxnet.sk>
 * @version 0.1.0
 *
 * @param {string} eventName
 * @param {function} listener
 * @param {*|null} element
 * @param {*|null} option
 * @return {function}
 * @deprecated
 */
function useEventListener (eventName, listener, element = window, { capture, once, passive } = {}) {
    const option = useMemo(function () {
        return { capture, once, passive }
    }, [capture, once, passive])

    useEffect(function () {
        if (!element?.addEventListener) return
        // add event listener and return removeEventListener callback
        return addEventListener(element, eventName, listener, option)
    }, [element, eventName, option])
}

export default useEventListener
