/**
 * Add event listener, and return remove callback.
 *
 * @author Tomáš Havlas <havlas@bbxnet.sk>
 * @version 0.1.0
 *
 * @param {*} element
 * @param {*} event
 * @param {function} callback
 * @param {*} option
 * @return {function}
 */
function addEventListener (element, event, callback, option = null) {
    if (!Array.isArray(event)) return addEventListener(element, [event], callback, option)

    for (let i = 0; i < event.length; i++)
        element.addEventListener(event[i], callback, option)

    return function () {
        for (let i = 0; i < event.length; i++)
            element.removeEventListener(event[i], callback, option)
    }
}

export default addEventListener
