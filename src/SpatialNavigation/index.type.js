import PropType from 'prop-types'

export default {
    /**
     * the children
     */
    children: PropType.node,
    /**
     * the default focusable component index
     */
    nextFocusableComponent: PropType.string.isRequired,
    /**
     * the default focusable component index, in reverse direction
     * default: $nextFocusableComponent
     */
    prevFocusableComponent: PropType.string,
}
