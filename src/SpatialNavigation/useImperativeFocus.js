import { useCallback } from 'react'

/**
 * The useImperativeFocus hook.
 *
 * @return {*}
 */
const useImperativeFocus = function () {
    return useCallback(function (index) {
        document.querySelector(`[data-focusable-component-index="${index}"]`)?.focus()
    }, [])
}

export default useImperativeFocus
