import useEventListener from '../WebTvNativeApp/useEventListener'
import { isNil } from 'ramda'
import getKeyboardEventKey from '../WebTvNativeApp/getKeyboardEventKey'
import propTypes from './index.type'

/**
 * The SpatialNavigation component.
 *
 * @return {JSX.Element}
 * @constructor
 */
function SpatialNavigation ({ children, nextFocusableComponent, prevFocusableComponent = nextFocusableComponent }) {
    useEventListener('keyup', function (event) {
        if (isNil(document.activeElement.dataset.focusableComponentIndex)) {
            if (getKeyboardEventKey(event) === 'ArrowRight' || getKeyboardEventKey(event) === 'ArrowDown') {
                document.querySelector(`[data-focusable-component-index="${nextFocusableComponent}"]`)?.focus()
            } else if (getKeyboardEventKey(event) === 'ArrowLeft' || getKeyboardEventKey(event) === 'ArrowUp') {
                document.querySelector(`[data-focusable-component-index="${prevFocusableComponent}"]`)?.focus()
            }
        }
    }, window)

    return children
}

SpatialNavigation.propTypes = propTypes

export default SpatialNavigation
