import { nullable } from '@havlasme/react-toolkit'
import cc from 'classcat'
import { isNil, times } from 'ramda'
import React from 'react'
import { v4 } from 'uuid'
import SpatialNavigation from '../SpatialNavigation'
import SpatialNavigationFocusable from '../SpatialNavigationFocusable'
import './style.scss'

const navMap = {
    0: { 'north': 6, 'south': 3, 'east': 1, 'west': 2 },
    1: { 'north': 7, 'south': 4, 'east': 2, 'west': 0 },
    2: { 'north': 8, 'south': 5, 'east': 0, 'west': 1 },
    3: { 'north': 0, 'south': 6, 'east': 4, 'west': 5 },
    4: { 'north': 1, 'south': 7, 'east': 5, 'west': 3 },
    5: { 'north': 2, 'south': 8, 'east': 3, 'west': 4 },
    6: { 'north': 3, 'south': 0, 'east': 7, 'west': 8 },
    7: { 'north': 4, 'south': 1, 'east': 8, 'west': 6 },
    8: { 'north': 5, 'south': 2, 'east': 6, 'west': 7 },
}

class TicTacToe extends React.Component {
    constructor (props) {
        super(props)

        this.state = {
            board: [null, null, null, null, null, null, null, null, null],
            connected: false,
            draw: false,
            id: null,
            move: false,
            opponent: null,
            session: null,
            win: null,
        }

        this.token = v4()

        this.ws = new WebSocket('ws://127.0.0.1:7071')
        this.ws.onopen = event => {
            this.setState({ connected: true })
            this.wssend({ cmd: 'lock', token: this.token })
        }
        this.ws.onclose = event => {
            this.setState({ connected: false })
        }

        this.ws.onmessage = event => {
            try {
                const msg = JSON.parse(event.data)

                console.log(msg)

                if (msg.cmd === 'lock:ack') {
                    this.setState({ id: msg.id })
                } else if (msg.cmd === 'host:ack') {
                    this.setState({ board: [null, null, null, null, null, null, null, null, null], session: msg.session })
                } else if (msg.cmd === 'connect:ack') {
                    this.setState({ board: [null, null, null, null, null, null, null, null, null], session: msg.session, opponent: msg.opponent, move: msg.move })
                } else if (msg.cmd === 'play:ack') {
                    this.setState(function (state) {
                        const board = [...state.board]
                        board[msg.index] = msg.id
                        return { board, draw: msg.draw, win: msg.win, move: msg.move }
                    })
                }
            } catch (e) {
                // do nothing
            }
        }

        this.connect = this.connect.bind(this)
        this.host = this.host.bind(this)
        this.play = this.play.bind(this)
        this.wssend = this.wssend.bind(this)
    }

    render () {
        return isNil(this.state.id) ? <div>LOADING</div> :
            isNil(this.state.session) ? (
                <div className="lobby">
                    <SpatialNavigation nextFocusableComponent="connect" prevFocusableComponent="connect">
                        <SpatialNavigationFocusable index="host" nextFocusableEast="connect" nextFocusableWest="connect">
                            <button type="button" onClick={this.host}>
                                HOST
                            </button>
                        </SpatialNavigationFocusable>

                        <SpatialNavigationFocusable index="connect" nextFocusableWest="host" nextFocusableEast="host">
                            <button autoFocus={true} type="button" onClick={this.connect}>
                                CONNECT
                            </button>
                        </SpatialNavigationFocusable>
                    </SpatialNavigation>
                </div>
            ) : (
                <SpatialNavigation nextFocusableComponent="0" prevFocusableComponent="0">
                    <div className="move">
                        {this.state.draw ? 'DRAW' :
                            !isNil(this.state.win) ? this.state.board[this.state.win[0]] === this.state.id ? 'WIN' : 'LOSE' :
                            this.state.move ? 'YOUR MOVE' : 'OPPONENT MOVE'
                        }
                    </div>

                    <div className={cc(['board', { draw: this.state.draw, win: !isNil(this.state.win) }])}>
                        {times((index) => {
                            return (
                                <SpatialNavigationFocusable index={index.toString()} key={index} nextFocusableNorth={navMap?.[index]?.north?.toString()} nextFocusableSouth={navMap?.[index]?.south?.toString()} nextFocusableEast={navMap?.[index]?.east?.toString()} nextFocusableWest={navMap?.[index]?.west?.toString()}>
                                    <button autoFocus={index === 0 && !isNil(this.state.opponent)} className={cc([{ win: this.state.win?.includes(index) && this.state.board[index] === this.state.id, lose: this.state.win?.includes(index) && this.state.board[index] !== this.state.id }])} disabled={isNil(this.state.opponent) || !isNil(this.state.win)} type="button" onClick={nullable(this.play, !isNil(this.state.board[index]))} data-index={index.toString()}>
                                        {!isNil(this.state.board[index]) && (this.state.board[index] === this.state.id ? 'X' : 'O')}
                                    </button>
                                </SpatialNavigationFocusable>
                            )
                        }, 9)}
                    </div>

                    {isNil(this.state.opponent) &&
                        <div className="waiting">
                            LOADING ...
                        </div>
                    }

                </SpatialNavigation>
            )

    }

    connect () {
        this.wssend({ cmd: 'connect' })
    }

    host () {
        this.wssend({ cmd: 'host' })
    }

    play (event) {
        this.wssend({ cmd: 'play', index: parseInt(event.target.dataset.index) })
    }

    wssend (message) {
        this.ws.send(JSON.stringify({ ...message, token: this.token }))
    }
}

export default TicTacToe
