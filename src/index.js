import React from 'react'
import ReactDOM from 'react-dom/client'
import GameBoard from './GameBoard'

const root = ReactDOM.createRoot(
    document.getElementById('web-root'),
)

root.render(<GameBoard/>)
