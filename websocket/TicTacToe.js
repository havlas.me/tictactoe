const uuid = require('uuid')
const { isNil } = require('ramda')

class TicTacToe {
    static HOST = 'host'
    static GUEST = 'guest'

    static WINMOVE = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ]

    constructor (host) {
        this.id = uuid.v4()
        this.board = [null, null, null, null, null, null, null, null, null]
        this.draw = false
        this.guest = null
        this.host = host
        this.move = TicTacToe.HOST
        this.win = null
    }

    check () {
        for (const move of TicTacToe.WINMOVE) {
            if (this.checkMove(...move)) {
                this.win = move
                break
            }
        }

        this.draw = this.board.reduce(function (draw, item) {
            return draw && !isNil(item)
        }, true)
    }

    checkMove (a, b, c) {
        return !isNil(this.board[a])
            && this.board[a] === this.board[b]
            && this.board[b] === this.board[c]
    }

    connect (id) {
        this.guest = id
    }

    opponent (id) {
        return this.host === id ? this.guest : this.host
    }

    play (index, id) {
        if (!isNil(this.win)) return false
        if (!isNil(this.board[index])) return false
        if (this[this.move] !== id) return false

        this.board[index] = this[this.move]
        this.move = this.move === TicTacToe.HOST ? TicTacToe.GUEST : TicTacToe.HOST

        this.check()

        return true
    }

    state () {
        return [this.win, this.draw]
    }
}

module.exports = TicTacToe
