const uuid = require('uuid')
const WebSocket = require('ws')
const { is } = require('ramda')
const wss = new WebSocket.Server({ port: 7071 })
const TicTacToe = require('./TicTacToe')

class ClientWS {
    constructor (ws, waitList) {
        this.id = uuid.v4()
        this.ws = ws
        this.waitList = waitList
        this.accessToken = null
        this.tictactoe = null

        ws.on('message', (message) => {
            try {
                this.command(JSON.parse(message))
            } catch (e) {
                console.warn('BAD COMMAND:', message)
            }
        })
    }

    authorize (accessToken) {
        return this.accessToken === accessToken
    }

    command (message) {
        if (is(Function, this[message.cmd])) {
            this[message.cmd](message)
        }
    }

    connect ({ cmd, token }) {
        if (!this.authorize(token)) return void 0

        const session = getRandomKey(this.waitList)
        this.tictactoe = this.waitList.get(session)
        this.waitList.delete(session)

        this.tictactoe.connect(this.id)

        const opponent = this.tictactoe.opponent(this.id)

        this.ws.send(JSON.stringify({ cmd: `${cmd}:ack`, session: session, opponent: opponent, move: false }))
        const host = clients.get(opponent)
        host.send(JSON.stringify({ cmd: `${cmd}:ack`, session: session, opponent: this.id, move: true }))
    }

    host ({ cmd, token }) {
        if (!this.authorize(token)) return void 0

        this.tictactoe = new TicTacToe(this.id)

        this.waitList.set(this.tictactoe.id, this.tictactoe)

        this.ws.send(JSON.stringify({ cmd: `${cmd}:ack`, session: this.tictactoe.id }))
    }

    lock ({ cmd, token }) {
        this.accessToken = token

        this.ws.send(JSON.stringify({ cmd: `${cmd}:ack`, id: this.id }))
    }

    play ({ cmd, index, token }) {
        if (!this.authorize(token)) return void 0
        if (!this.tictactoe.play(index, this.id)) return void 0

        const [win, draw] = this.tictactoe.state()

        this.ws.send(JSON.stringify({ cmd: `${cmd}:ack`, id: this.id, draw, index, win, move: false }))
        const opponent = clients.get(this.tictactoe.opponent(this.id))
        opponent.send(JSON.stringify({ cmd: `${cmd}:ack`, id: this.id, draw, index, win, move: true }))
    }
}

function getRandomKey (collection) {
    let index = Math.floor(Math.random() * collection.size)
    let cntr = 0
    for (let key of collection.keys()) {
        if (cntr++ === index) {
            return key
        }
    }
}

const clients = new Map()
const waiting = new Map()

wss.on('connection', (ws) => {
    const client = new ClientWS(ws, waiting)

    clients.set(client.id, ws)

    ws.on('close', () => {
        clients.delete(client.id)
    })
})
